<?
$dbcfg = array(
    'host' => 'localhost',
    'port' => '3306',
    'dbname' => 'site-db',
    'username' => 'root',
    'password' => ''
);

// Connection code here!
$dbh = null;
try {
    $dbh = new PDO('mysql:host=' . $dbcfg['host'] . ';port=' . $dbcfg['port'] . ';dbname=' . $dbcfg['dbname'],
    $dbcfg['username'], $dbcfg['password']);
} catch (PDOException $e) {
    die();
}
$GLOBALS['dbh'] = $dbh;
?>