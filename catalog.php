<?
include_once __DIR__ . '/functions.php';
include_once __DIR__ . '/config/db.php';

$data = null;
$getData = $dbh->prepare("SELECT * FROM `settings` WHERE `id`='1';");
$getData->execute();
$datas = $getData->fetchAll();
foreach($datas as $v) {
  $data = $v;
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><? he($data['center_title']); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">

    <!-- Plugin CSS -->
    <link href="/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/css/main.min.css" rel="stylesheet">

  </head>

  <body id="catalog-body">
    <h1 id="main-cat-title">Каталог</h1>
    <div class="container" id="main-cat-container">
      <div class="loader-bg" v-if="submitting == true">
        <div class="loader"></div>
      </div>
      <div id="catalog-menu" class="row">
        <div class="col-12">
          <div id="catalog-menu-links">
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link" href="/">Главная</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="/catalog.php">Каталог</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/#portfolio">Деятельность</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/#about">О нас</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/#contact">Контакты</a>
              </li>
            </ul>
          </div>
          <div id="catalog-menu-checkoutbtn">
            <button class="btn btn-outline-success btn-sm" @click="showCheckout(1)" v-if="checkout == 0">Оформить заказ</button>
            <button class="btn btn-outline-info btn-sm" @click="showCheckout(0)" v-if="checkout == 1">Отложить оформление</button>
          </div>
        </div>
      </div>
      <div class="row" v-if="checkout == 0">
        <div class="col-xl-3 p-3">
          <ul class="list-group">
            <li @click="switchCat(0)" v-bind:class="[category == 0 ? catClass + ' active' : catClass]">Любая</li>
            <li v-for="cat in categories" :key="cat.id" v-bind:class="[category == cat.id ? catClass + ' active' : catClass]" @click="switchCat(cat.id)">{{cat.name}}</li>
          </ul>
        </div>
        <div class="col-xl-9 p-3 d-flex flex-column">
          <div class="w-100 d-flex flex-row flex-wrap" style="perspective: 600px;">
            <div class="product-card m-2" v-for="product in products" :key="product.id">
              <div class="product-card__face product-card-front" v-bind:style="{ 'background-image': 'url(' + product.imgsrc + ')' }"></div>
              <div class="product-card__face product-card-back">
                <h2 class="product-card-text product-card-name">{{product.name}}</h2>
                <p class="product-card-text product-card-category">{{product.category.name}}</p>
                <p class="product-card-text product-card-cost"><span>Цена:</span> {{product.cost}}</p>
                <button class="btn btn-sm mt-3 btn-outline-success" @click="addToCart(product.id)">В корзину</button>
              </div>
            </div>
          </div>
          <div class="w-100 d-flex flex-row align-items-center justify-content-center" style="height:60px">
            <nav aria-label="Навигация">
              <ul class="pagination">
                  <li v-bind:class="[page == 1 ? 'page-item disabled' : 'page-item']"><a class="page-link" @click="previousPage">&lt;&lt; Назад</a></li>
                  <li v-bind:class="[page == totalPages ? 'page-item disabled' : 'page-item']"><a class="page-link" @click="nextPage">Вперёд &gt;&gt;</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="container" v-if="checkout == 1">
        <table id="cart" class="table table-hover table-condensed">
                  <thead>
                  <tr>
                    <th style="width:50%">Продукт</th>
                    <th style="width:10%">Цена</th>
                    <th style="width:8%">Количество</th>
                    <th style="width:22%" class="text-center">Итого</th>
                    <th style="width:10%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in cart">
                    <td v-bind:data-th="item.name">
                      <div class="row">
                        <div class="col-lg-3 col-sm-6 hidden-xs"><img v-bind:src="item.imgsrc" alt="Картинка" width="100" height="100" class="img-responsive"/></div>
                        <div class="col-lg-9 col-sm-6">
                          <h4 class="nomargin">{{item.name}}</h4>
                          <p><b>Категория: </b>{{item.category.name}}</p>
                        </div>
                      </div>
                    </td>
                    <td data-th="Цена">{{item.cost}} руб.</td>
                    <td data-th="Количество">
                      <input type="number" class="form-control text-center" min="1" v-model:value="item.count" @change="saveCart">
                    </td>
                    <td data-th="Итого" class="text-center">{{item.count * item.cost}} руб.</td>
                    <td class="actions" data-th="">
                      <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Удалить" @click="removeFromCart(item.id, true)"><i class="fa fa-trash-o"></i></button>								
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td><button class="btn btn-warning" @click="showCheckout(0)"><i class="fa fa-angle-left"></i> Продолжить покупки</button></td>
                    <td colspan="2" class="hidden-xs"></td>
                    <td class="hidden-xs text-center"><strong>Итого: {{total}} руб.</strong></td>
                    <td><a href="#" class="btn btn-success btn-block" @click="showCheckout(2)">Оформить <i class="fa fa-angle-right"></i></a></td>
                  </tr>
                </tfoot>
              </table>
        </div>
        <div class="container" v-if="checkout == 2">
          <form class="mt-4" style="max-width: 600px; margin: 0 auto;" v-on:submit.prevent="sendOrder">
            <div class="form-group">
              <label for="name-input">Имя</label>
              <input type="text" v-model="cName" class="form-control" id="name-input" placeholder="Введите ваше имя">
            </div>
            <div class="form-group">
              <label for="email-input">Адрес E-Mail</label>
              <input type="email" v-model="cEmail" class="form-control" id="email-input" placeholder="Введите ваш E-Mail">
            </div>
            <div class="form-group">
              <label for="phone-input">Телефон</label>
              <input type="text" v-model="cPhone" class="form-control" id="phone-input" placeholder="Введите ваш телефон">
            </div>
            <button type="submit" class="btn btn-primary">Отправить заказ</button>
          </form>
        </div>
        <div class="container mt-4" v-if="checkout == 3">
          <div v-bind:class="[checkout_success ? 'alert alert-success' : 'alert alert-danger']" role="alert">
            {{checkout_success ? 'Заказ успешно зарегистрирован' : 'Не удалось зарегистрировать заказ'}}
          </div>
        </div>
    </div>
    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/vendor/vue/vue.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/catalog.min.js"></script>

  </body>

</html>
