<?
session_start();
include_once __DIR__ . '/admin/checkauth.php';
include_once __DIR__ . '/config/db.php';
include_once __DIR__ . '/functions.php';

if (!is_authorized($dbh)) {
    header('Location: /auth.php');
    exit();
}

$title = null;
if (par('contact')) {
    $title = 'Связь';
}

if (par('settings')) {
    $title = 'Параметры';
}

if (par('orders')) {
    $title = 'Заказы';
}

if (par('products')) {
    $title = 'Продукты';
}

if (par('feedback') || empty($title)) {
    $title = 'Отзывы';
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?he($title);?> - ООО &#10077;Контроль&#10078;</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">

    <!-- Plugin CSS -->
    <link href="/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/css/main.min.css" rel="stylesheet">

  </head>

  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase navbar-shrink" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Админпанель</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Навигация">
          Меню
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded <?if (par('feedback') || (!par('contact') && !par('settings') && !par('orders') && !par('products'))) {
    he('active');
}
?>" href="/admin.php?feedback">Отзывы</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded <?if (par('contact')) {
    he('active');
}
?>" href="/admin.php?contact">Связь</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded <?if (par('orders')) {
    he('active');
}
?>" href="/admin.php?orders">Заказы</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded <?if (par('products')) {
    he('active');
}
?>" href="/admin.php?products">Продукты</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded <?if (par('settings')) {
    he('active');
}
?>" href="/admin.php?settings">Параметры</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded" href="/auth.php?logout">Выход</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container admin">
    <?
if (par('feedback')) {
    include_once __DIR__ . '/admin/feedback.php';
} else if (par('contact')) {
    include_once __DIR__ . '/admin/contact.php';
} else if (par('settings')) {
    include_once __DIR__ . '/admin/settings.php';
} else if (par('orders')) {
    include_once __DIR__ . '/admin/orders.php';
} else if (par('products')) {
    include_once __DIR__ . '/admin/products.php';
} else {
    include_once __DIR__ . '/admin/feedback.php';
}

?>
    </div>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/vendor/vue/vue.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/admin.min.js"></script>

  </body>

</html>
