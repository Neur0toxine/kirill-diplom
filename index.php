<?
include_once __DIR__ . '/functions.php';
include_once __DIR__ . '/config/db.php';

$data = null;
$getData = $dbh->prepare("SELECT * FROM `settings` WHERE `id`='1';");
$getData->execute();
$datas = $getData->fetchAll();
foreach($datas as $v) {
  $data = $v;
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><? he($data['center_title']); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">

    <!-- Plugin CSS -->
    <link href="/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/css/main.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><? he($data['title']); ?></a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Навигация">
          Меню
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded" href="/catalog.php">Каталог</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Деятельность</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">О нас</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Контакты</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">
      <div class="container title-container">
        <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
        <h1 class="text-uppercase mb-0 title-shadow"><? he($data['center_title']); ?></h1>
        <hr>
        <h2 class="font-weight-light mb-0 subtitle-shadow"><? he($data['subtitle']); ?></h2>
      </div>
    </header>

    <!-- Company Portfolio Grid Section -->
    <section class="portfolio" id="portfolio">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Деятельность</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="activity-block">
            <img src="<? he($data['act_img1']) ?>" alt="" class="img-thumbnail activity-image">
            <div class="activity-text-container">
              <p class="activity-text"><? e($data['act_text1']) ?></p>
            </div>
          </div>
          <hr class="activity-spacer">
          <div class="activity-block img-right">
            <img src="<? he($data['act_img2']) ?>" alt="" class="img-thumbnail activity-image">
            <div class="activity-text-container">
              <p class="activity-text"><? e($data['act_text2']) ?></p>
            </div>
          </div>
          <hr class="activity-spacer">
          <div class="activity-block">
            <img src="<? he($data['act_img3']) ?>" alt="" class="img-thumbnail activity-image">
            <div class="activity-text-container">
              <p class="activity-text"><? e($data['act_text3']) ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- About Section -->
    <section class="bg-primary text-white mb-0" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">О нас</h2>
        <hr class="star-light mb-5">
        <div class="row">
          <div class="col-lg-4 ml-auto">
            <p class="lead"><? e(nl2br(h($data['left_descr']))); ?></p>
          </div>
          <div class="col-lg-4 mr-auto">
            <p class="lead"><? e(nl2br(h($data['right_descr']))); ?></p>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Связаться с нами</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
            <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
            <ul class="nav nav-tabs" id="contactTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="request-tab" data-toggle="tab" href="#request" role="tab" aria-controls="request" aria-selected="false">Оставить сообщение</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="feedback-tab" data-toggle="tab" href="#feedback" role="tab" aria-controls="feedback" aria-selected="true">Оставить отзыв</a>
                </li>
              </ul>
              <div class="tab-content" id="contactTabContent">
                <div class="tab-pane fade show active" id="request" role="tabpanel" aria-labelledby="request-tab">
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Как к Вам обращаться?</label>
                            <input class="form-control" id="name" type="text" placeholder="Имя" required="required" data-validation-required-message="Пожалуйста, укажите Ваше имя">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Введите здесь свой E-Mail для связи</label>
                            <input class="form-control" id="email" type="email" placeholder="Адрес E-Mail" required="required" data-validation-required-message="Пожалуйста, введите Ваш личный или корпоративный E-Mail, иначе мы не сможем с вами связаться!">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Мы хотим позвонить Вам</label>
                            <input class="form-control" id="phone" type="tel" placeholder="Номер телефона" required="required" data-validation-required-message="Введите свой номер телефона">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Пишите здесь</label>
                            <textarea class="form-control" id="message" rows="5" placeholder="Введите своё сообщение" required="required" data-validation-required-message="Простите, но без сообщения никак нельзя!"></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Отправить</button>
                        </div>
                      </form>
                </div>
                <div class="tab-pane fade" id="feedback" role="tabpanel" aria-labelledby="feedback-tab">
                    <form name="sentFeedback" id="feedbackForm" novalidate="novalidate">
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Как к Вам обращаться?</label>
                            <input class="form-control" id="uname" type="text" placeholder="Имя" required="required" data-validation-required-message="Пожалуйста, укажите Ваше имя">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Мы готовы выслушать Ваши предложения</label>
                            <textarea class="form-control" id="feedback-message" rows="5" placeholder="Введите своё сообщение" required="required" data-validation-required-message="Простите, но без сообщения никак нельзя!"></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <br>
                        <div id="success_fb"></div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-xl" id="sendFeedbackButton">Отправить</button>
                        </div>
                      </form>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container mt-5 mb-1">
        <h3 class="text-center text-uppercase text-secondary mb-0">Контактные данные</h3>
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <table class="table mt-2">
              <tbody>
                <tr>
                  <td><b>E-Mail</b></td>
                  <td><a href="mailto:<? he($data['email']); ?>"><? he($data['email']); ?></a></td>
                </tr>
                <tr>
                  <td><b>Телефон</b></td>
                  <td><a href="tel:<? he($data['phone']); ?>"><? he(format_phone($data['phone'])); ?></a></td>
                </tr>
                <tr>
                  <td><b>Телефон/факс</b></td>
                  <td><a href="tel:<? he($data['fax']); ?>"><? he(format_phone($data['fax'])); ?></a></td>
                </tr>
                <tr>
                  <td><b>Мобильный</b></td>
                  <td><a href="tel:<? he($data['mobile']); ?>"><? he(format_phone($data['mobile'])); ?></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Наш адрес</h4>
            <p class="lead mb-0"><? e(nl2br(h($data['address']))); ?></p>
          </div>
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4<? if(empty($data['vk']) && empty($data['facebook']) && empty($data['googleplus']) && empty($data['twitter']) && empty($data['linkedin'])) he(' hidden'); ?>">Мы в Сети</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item<? if(empty($data['vk'])) he(' hidden'); ?>">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="<? he($data['vk']); ?>">
                  <i class="fa fa-fw fa-vk"></i>
                </a>
              </li>
              <li class="list-inline-item<? if(empty($data['facebook'])) he(' hidden'); ?>">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="<? he($data['facebook']); ?>">
                  <i class="fa fa-fw fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item<? if(empty($data['googleplus'])) he(' hidden'); ?>">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="<? he($data['googleplus']); ?>">
                  <i class="fa fa-fw fa-google-plus"></i>
                </a>
              </li>
              <li class="list-inline-item<? if(empty($data['twitter'])) he(' hidden'); ?>">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="<? he($data['twitter']); ?>">
                  <i class="fa fa-fw fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item<? if(empty($data['linkedin'])) he(' hidden'); ?>">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="<? he($data['linkedin']); ?>">
                  <i class="fa fa-fw fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <h4 class="text-uppercase mb-4">О нас</h4>
            <p class="lead mb-0"><? e(nl2br(h($data['short_descr']))); ?></p>
          </div>
        </div>
      </div>
    </footer>

    <div class="copyright py-4 text-center text-white">
      <div class="container">
        <small>Copyright &copy; <? e($data['center_title']); ?> <? he(date("Y")); ?></small>
      </div>
    </div>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact and Feedback Forms JavaScript -->
    <script src="/js/jqBootstrapValidation.min.js"></script>
    <script src="/js/feedback.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/main.min.js"></script>

  </body>

</html>
