<?php
session_start();    // start session
require_once __DIR__ . '/../config/db.php';
include_once __DIR__ . '/../admin/checkauth.php';

function json($array_to_send) {
    echo json_encode($array_to_send);
}

function random_filename($length, $directory = '', $extension = '')
{
    // default to this files directory if empty...
    $dir = !empty($directory) && is_dir($directory) ? $directory : dirname(__FILE__);

    do {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    } while (file_exists($dir . '/' . $key . (!empty($extension) ? '.' . $extension : '')));

    return $key . (!empty($extension) ? '.' . $extension : '');
}

// Decline request without authentication
function onlyAuthorized() {
    if(!is_authorized($GLOBALS['dbh'])) {
        json([
            'error' => 'Authentication cookie not found!'
        ]);
        exit();
    }
}

// Deny GET requests.
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    echo 'This route accepts only POST request with JSON commands.';
    exit();
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES['imgfile'])) {
    $sep = DIRECTORY_SEPARATOR;
    $upload_dir = realpath(__DIR__ . $sep . '..' . $sep . 'img' . $sep . 'uploaded' . $sep . 'products' . $sep);
    $filename = $_POST['img_name'];
    if(empty($filename)) {
        $filename = random_filename('15', $upload_dir);
    }
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $accepted_types = ['image/png' => 'png'];
    if(empty($_FILES['imgfile']['tmp_name'])) exit();
    $mime = finfo_file($finfo, $_FILES['imgfile']['tmp_name']);
    if(isset($accepted_types[$mime])) {
        $uploadfile = $upload_dir . $sep . basename($filename . '.' . $accepted_types[$mime]);
        move_uploaded_file($_FILES['imgfile']['tmp_name'], $uploadfile);
        echo 'ok:/img/uploaded/products/' . $filename . '.' . $accepted_types[$mime];
    } else {
        echo 'error: this mimetype is not allowed';
    }
    exit();
}

// All other requests will be in JSON format.
header("Content-Type: application/json");

// Parse request JSON.
$request = null;
try {
    $request = json_decode(file_get_contents("php://input"));
}
catch(Exception $e) {
    json([
        'error' => 'Incorrect JSON in request'
    ]);
    exit();
}

// Catch empty requests.
if (empty($request)) {
    json([
        'error' => 'Empty request provided'
    ]);
    exit();
}

// Controller for our route.
class ProductsController {
    function create($req) {
        $req = (array) json_decode(file_get_contents("php://input"), TRUE)['data'];
        if(empty($req['name'])) return $this->error('Name is not provided');
        if(empty($req['category'])) return $this->error('Category is not provided');
        if(empty($req['cost'])) return $this->error('Cost is not provided');
        if(empty($req['count'])) return $this->error('Count is not provided');
        if(empty($req['imgsrc'])) return $this->error('Image link is not provided');
        try {
            $stmt = $GLOBALS['dbh']->prepare('INSERT INTO items(`name`, `category`, `cost`, `count`, `imgsrc`) VALUES(:name, :category, :cost, :count, :imgsrc)');
            $stmt->execute([
                'name' => $req['name'],
                'category' => $req['category'],
                'cost' => $req['cost'],
                'count' => $req['count'],
                'imgsrc' => $req['imgsrc']
            ]);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function read($req) {
        if(empty($req->page)) return $this->error('Page is not provided');
        if(empty($req->limit)) return $this->error('Limit is not provided');
        try {
            $page = intval($req->page);
            $limit = intval($req->limit);
            $offset = ($page - 1) * $limit;
            $countreq = 'SELECT count(*) FROM `items`';
            if(!empty($req->category)) {
                $countreq .= ' WHERE `category`=' . intval($req->category);
            }
            $xtractreq = 'SELECT `categories`.`id` AS `cat_id`, `categories`.`name` AS `cat_name`, `items`.* FROM `categories`,`items` WHERE `categories`.`id`=`items`.`category`';
            if(!empty($req->category)) {
                $xtractreq .= ' AND `categories`.`id`=' . intval($req->category);
            }
            $xtractreq .= ' LIMIT ' . $offset . ',' . $limit;
            $items = [];
            $totalRows = intval($GLOBALS['dbh']->query($countreq)->fetch()[0]);
            $result = $GLOBALS['dbh']->query($xtractreq)->fetchAll();
            $pagesCount = ceil($totalRows / $limit);
            foreach($result as $it) {
                array_push($items, [
                    'id' => intval($it['id']),
                    'name' => $it['name'],
                    'category' => [
                        'id' => intval($it['cat_id']),
                        'name' => $it['cat_name']
                    ],
                    'cost' => $it['cost'],
                    'count' => intval($it['count']),
                    'imgsrc' => $it['imgsrc']
                ]);
            }
            return [
                'page' => $page,
                'totalPages' => $pagesCount,
                'limit' => $limit,
                'totalItems' => $totalRows,
                'items' => $items
            ];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function update($req) {
        if(empty($req->id)) return $this->error('ID is not provided');
        if(empty($req->name)) return $this->error('Name is not provided');
        if(empty($req->category)) return $this->error('Category is not provided');
        if(empty($req->cost)) return $this->error('Cost is not provided');
        if(empty($req->count)) return $this->error('Count is not provided');
        try {
            $query = 'UPDATE `items` SET `name`=:name, `category`=:category, `cost`=:cost, `count`=:count ' . (empty($req->imgsrc) ? '' : ', `imgsrc`=:imgsrc') . ' WHERE `id`=:id';
            $stmt = $GLOBALS['dbh']->prepare($query);
            $arr = [
                'id' => intval($req->id),
                'name' => $req->name,
                'category' => intval($req->category),
                'cost' => $req->cost,
                'count' => intval($req->count),
            ];
            if(!empty($req->imgsrc)) $arr['imgsrc'] = $req->imgsrc;
            $stmt->execute($arr);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function delete($req) {
        if(empty($req->id)) return $this->error('ID is not provided');
        try {
            $stmt = $GLOBALS['dbh']->prepare('DELETE FROM `items` WHERE `id`=:id');
            $stmt->execute([
                'id' => intval($req->id)
            ]);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function error($msg) {
        return ['error' => $msg];
    }
}

class CategoriesController {
    function create($req) {
        if(empty($req->name)) return $this->error('Name is not provided');
        try {
            $stmt = $GLOBALS['dbh']->prepare('INSERT INTO `categories`(`name`) VALUES(:name)');
            $stmt->execute([
                'name' => $req->name
            ]);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function read($req) {
        $categories = [];
        $result = $GLOBALS['dbh']->query('SELECT * FROM `categories`')->fetchAll();
        foreach($result as $v) {
            array_push($categories, [
                'id' => $v['id'],
                'name' => $v['name']
            ]);
        }
        return ['items' => $categories];
    }
    function update($req) {
        if(empty($req->id)) return $this->error('ID is not provided');
        if(empty($req->name)) return $this->error('Name is not provided');
        try {
            $stmt = $GLOBALS['dbh']->prepare('UPDATE `categories` SET `name`=:name WHERE `id`=:id');
            $stmt->execute([
                'id' => intval($req->id),
                'name' => $req->name
            ]);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function delete($req) {
        if(empty($req->id)) return $this->error('ID is not provided');
        try {
            $stmt = $GLOBALS['dbh']->prepare('DELETE FROM `categories` WHERE `id`=:id');
            $stmt->execute([
                'id' => intval($req->id)
            ]);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function error($msg) {
        return ['error' => $msg];
    }
}

$products = new ProductsController();
$cats = new CategoriesController();

if(empty($request->data)) {
    json(['error' => 'Empty data provided']);
    exit();
}

switch($request->action) {
    case 'create':
        onlyAuthorized();
        json($products->create($request->data));
        break;
    case 'read':
        json($products->read($request->data));
        break;
    case 'update': 
        onlyAuthorized();
        json($products->update($request->data));
        break;
    case 'delete':
        onlyAuthorized();
        json($products->delete($request->data));
        break;
    case 'cat_create':
        onlyAuthorized();
        json($cats->create($request->data));
        break;
    case 'cat_read':
        json($cats->read($request->data));
        break;
    case 'cat_update': 
        onlyAuthorized();
        json($cats->update($request->data));
        break;
    case 'cat_delete':
        onlyAuthorized();
        json($cats->delete($request->data));
        break;
    default:
        json(['error' => 'Unknown action type']);
        break;
}
?>