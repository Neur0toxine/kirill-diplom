<?php
include_once '../config/db.php';

// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['phone']) 		||
   empty($_POST['message'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "Incorrect data provided!";
	return false;
   }
	
$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));

$statement = $dbh->prepare("INSERT INTO contact(name, email, phone, text)
    VALUES(:name, :email, :phone, :message)");

$statement->execute(array(
    "name" => $name,
    "email" => $email_address,
    "phone" => $phone,
    "message" => $message
));
return true;			
?>
