<?php
include_once '../config/db.php';

// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['message']))
   {
	echo "Incorrect data provided!";
	return false;
   }
	
$name = strip_tags(htmlspecialchars($_POST['name']));
$message = strip_tags(htmlspecialchars($_POST['message']));

$statement = $dbh->prepare("INSERT INTO feedback(name, text)
    VALUES(:name, :message)");

$statement->execute(array(
    "name" => $name,
    "message" => $message
));
return true;			
?>
