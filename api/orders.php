<?php
session_start();    // start session
require_once __DIR__ . '/../config/db.php';
include_once __DIR__ . '/../admin/checkauth.php';

function json($array_to_send) {
    echo json_encode($array_to_send);
}

// Decline request without authentication
function onlyAuthorized() {
    if(!is_authorized($GLOBALS['dbh'])) {
        json([
            'error' => 'Authentication cookie not found!'
        ]);
        exit();
    }
}

// Deny GET requests.
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    echo 'This route accepts only POST request with JSON commands.';
    exit();
}

// All other requests will be in JSON format.
header("Content-Type: application/json");

// Parse request JSON.
$request = null;
try {
    $request = json_decode(file_get_contents("php://input"));
}
catch(Exception $e) {
    json([
        'error' => 'Incorrect JSON in request'
    ]);
    exit();
}

// Catch empty requests.
if (empty($request)) {
    json([
        'error' => 'Empty request provided'
    ]);
    exit();
}

// Controller for our route.
class OrdersController {
    function create($req) {
        $req = (array) json_decode(file_get_contents("php://input"), TRUE)['data'];
        if(empty($req['name'])) return $this->error('Name is not provided');
        if(empty($req['email'])) return $this->error('E-Mail is not provided');
        if(empty($req['phone'])) return $this->error('Phone is not provided');
        if(empty($req['products'])) return $this->error('Products is not provided');
        $GLOBALS['dbh']->beginTransaction();
        try {
            $stmt = $GLOBALS['dbh']->prepare('INSERT INTO orders(`name`, `email`, `phone`, `status`) VALUES(:name, :email, :phone, 0)');
            $stmt->execute([
                'name' => $req['name'],
                'email' => $req['email'],
                'phone' => $req['phone']
            ]);
            $order_id = $GLOBALS['dbh']->lastInsertId();
            foreach($req['products'] as $product) {
                $stmt = $GLOBALS['dbh']->prepare('INSERT INTO ordered_items(`name`, `category`, `cost`, `count`, `order_id`) VALUES (:name, :category, :cost, :count, ' . $order_id . ')');
                $stmt->execute([
                    'name' => $product['name'],
                    'category' => $product['category']['name'],
                    'cost' => $product['cost'],
                    'count' => $product['count']
                ]);
            }
            $GLOBALS['dbh']->commit();
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            $GLOBALS['dbh']->rollBack();
            return ['success' => false];
        }
    }
    function read($req) {
        if(empty($req->page)) return $this->error('Page is not provided');
        if(empty($req->limit)) return $this->error('Limit is not provided');
        try {
            $page = intval($req->page);
            $limit = intval($req->limit);
            $offset = ($page - 1) * $limit;
            $countreq = 'SELECT count(*) FROM `orders`';
            if(isset($req->status)) {
                $countreq .= ' WHERE `status`=' . intval($req->status);
            }
            $xtractreq = 'SELECT * FROM `orders`';
            if(isset($req->status)) {
                $xtractreq .= ' WHERE `status`=' . intval($req->status);
            }
            $xtractreq .= ' ORDER BY `created_at` DESC LIMIT ' . $offset . ',' . $limit;
            $items = [];
            $totalRows = intval($GLOBALS['dbh']->query($countreq)->fetch()[0]);
            $result = $GLOBALS['dbh']->query($xtractreq)->fetchAll();
            $pagesCount = ceil($totalRows / $limit);
            foreach($result as $it) {
                $products = [];
                $price = 0;
                $db_items = $GLOBALS['dbh']->query('SELECT `name`, `category`, `cost`, `count`, `order_id` FROM `ordered_items` WHERE `order_id`=' . $it['id'])->fetchAll();
                foreach($db_items as $item) {
                    $price += $item['cost'] * $item['count'];
                    array_push($products, [
                        'name' => $item['name'],
                        'category' => $item['category'],
                        'cost' => $item['cost'],
                        'count' => $item['count'],
                        'price' => $item['cost'] * $item['count']
                    ]);
                }
                array_push($items, [
                    'id' => intval($it['id']),
                    'created' => $it['created_at'],
                    'customer_name' => $it['name'],
                    'customer_email' => $it['email'],
                    'products' => $products,
                    'total' => $price,
                    'status' => intval($it['status'])
                ]);
            }
            return [
                'page' => $page,
                'totalPages' => $pagesCount,
                'limit' => $limit,
                'totalItems' => $totalRows,
                'items' => $items
            ];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function update($req) {
        if(empty($req->id)) return $this->error('ID is not provided');
        if(empty($req->status)) return $this->error('Status is not provided');
        try {
            $query = 'UPDATE `orders` SET `status`=:status WHERE `id`=:id';
            $stmt = $GLOBALS['dbh']->prepare($query);
            $arr = [
                'id' => intval($req->id),
                'status' => intval($req->status)
            ];
            $stmt->execute($arr);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function delete($req) {
        if(empty($req->id)) return $this->error('ID is not provided');
        try {
            $stmt = $GLOBALS['dbh']->prepare('DELETE FROM `orders` WHERE `id`=:id');
            $stmt->execute([
                'id' => intval($req->id)
            ]);
            return ['success' => true];
        }
        catch(PDOException $e) {
            $this->error($e->getMessage());
            return ['success' => false];
        }
    }
    function error($msg) {
        return ['error' => $msg];
    }
}

$orders = new OrdersController();

if(empty($request->data)) {
    json(['error' => 'Empty data provided']);
    exit();
}

switch($request->action) {
    case 'create':
        json($orders->create($request->data));
        break;
    case 'read':
        onlyAuthorized();
        json($orders->read($request->data));
        break;
    case 'update': 
        onlyAuthorized();
        json($orders->update($request->data));
        break;
    case 'delete':
        onlyAuthorized();
        json($orders->delete($request->data));
        break;
    default:
        json(['error' => 'Unknown action type']);
        break;
}
?>