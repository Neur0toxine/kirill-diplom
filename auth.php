<?
include_once __DIR__ . '/admin/checkauth.php';
include_once __DIR__ . '/functions.php';
include_once __DIR__ . '/config/db.php';

if(session_status() == PHP_SESSION_NONE) {
    session_start([
        'cookie_secure' => 0,
        'cookie_httponly' => 1,
        'cookie_lifetime' => 31557600,
        'use_strict_mode' => 1,
        'use_trans_sid' => 1
    ]);
}

if($_SERVER['REQUEST_METHOD'] == 'GET') {
    if(par('logout')) {
        unset($_SESSION['hash']);
        unset($_SESSION['login']);
        session_unset();
        session_destroy();
        header('Location: /index.php');
        exit();
    }
}

if(is_authorized($dbh)) {
    header('Location: /admin.php');
    exit();
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = '';
    $password = _post('password');
    $data = null;
    $getData = $dbh->prepare("SELECT `password` FROM `settings` WHERE `id`='1';");
    $getData->execute();
    $datas = $getData->fetchAll();
    foreach($datas as $v) {
        $data = $v;
    }
    if($password == $data['password']) {
        $_SESSION['login'] = '1';
        $_SESSION['hash'] = md5(md5($data['password']));
        header('Location: /admin.php');
        exit();
    }
    else {
        $error = '<div class="alert alert-danger mb-1" role="alert">Неверный пароль!</div>';
    }
}
?>
<!DOCTYPE html>
<html>
 <head>
  <title>Авторизация</title>
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=cyrillic" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
  <style>
   h1 {
       font-family: 'Montserrat', sans-serif;
   }

   .form {
       margin: 0 auto;
       width: 100%;
       height: 100vh;
       display: flex;
       flex-direction: column;
       align-items: center;
       justify-content: center;
   }

   form {
       width: 250px;
   }
  </style>
 </head>
 <body>
  <div class="form">
   <h1>Авторизация</h1>
   <form action="/auth.php" method="post">
    <input type="password" name="password" class="form-control mb-1" placeholder="Введите пароль">
    <? e($error); ?>
    <input type="submit" class="btn btn-primary w-100" value="Вход">
   </form>
  </div>
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
 </body>
</html>