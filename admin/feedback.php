<?
session_start();
include_once __DIR__ . '/checkauth.php';
include_once __DIR__ . '/../config/db.php';
include_once __DIR__ . '/../functions.php';

if(!is_authorized($dbh)) {
    header('Location: /auth.php');
    exit();
}

if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $req = [];
    parse_str(file_get_contents("php://input"), $req);
    $stmt = $dbh->prepare('DELETE FROM `feedback` WHERE `id`=:id');
    try {
        $stmt->execute(['id' => $req['id']]);
    }
    catch(PDOException $e) {
        header('HTTP/1.1 500 Internal Server Error');
    }
    exit();
}

$page = _get('page');
if(empty($page)) $page = 1;
$perPage = 10;
$offset = ($page - 1) * $perPage;

$getData = $dbh->query("SELECT * FROM `feedback` ORDER BY `created_at` DESC LIMIT " . $perPage . " OFFSET " . $offset);
$datas = $getData->fetchAll();

foreach($datas as $v) {
    e('<div class="admin-card feedback" data-id="' . h($v['id']) . '">');
    e('<div class="closebtn">');
    e('<i class="fa fa-times"></i>');
    e('</div>');
    e('<table class="table">');
    e('<tbody>');
    e('<tr>');
    e('<th scope="row">Отправлено</th>');
    e('<td>' . h($v['created_at']) . '</td>');
    e('</tr>');
    e('<tr>');
    e('<th scope="row">Имя</th>');
    e('<td>' . h($v['name']) . '</td>');
    e('</tr>');
    e('<tr>');
    e('<th scope="row">Отзыв</th>');
    e('<td>' . nl2br(h($v['text'])) . '</td>');
    e('</tr>');
    e('</tbody>');
    e('</table>');
    e('</div>');
}
?>

<div class="d-flex flex-row align-items-center justify-content-center">
    <nav aria-label="Навигация">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="<? e('/admin.php?feedback&page=' . ($page-1)) ?>">&lt;&lt; Назад</a></li>
            <li class="page-item"><a class="page-link" href="<? e('/admin.php?feedback&page=' . ($page+1)) ?>">Вперёд &gt;&gt;</a></li>
        </ul>
    </nav>
</div>