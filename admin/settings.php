<?
session_start();
include_once __DIR__ . '/checkauth.php';
include_once __DIR__ . '/../config/db.php';
include_once __DIR__ . '/../functions.php';

if(!is_authorized($dbh)) {
    header('Location: /auth.php');
    exit();
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    switch(_post('actiontype')) {
        case 'password-update':
            $newpassword = _post('newpassword');
            if($newpassword != _post('newpassword2')) {
                header('Location: /admin.php?settings&error1=1#form1'); 
                break;
            }
            $stmt = $dbh->prepare("UPDATE `settings` SET `password`=:password");
            try {
                $stmt->execute(['password' => $newpassword]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error1#form1'); 
                break;
            }
            header('Location: /admin.php?settings&success1#form1'); 
            break;   
        case 'titles-update':
            $title = _post('title');
            $center_title = _post('center_title');
            $subtitle = _post('subtitle');
            $sep = DIRECTORY_SEPARATOR;
            $upload_dir = realpath(__DIR__ . $sep . '..' . $sep . 'img' . $sep);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $accepted_types = [
                'image/png' => 'png'
            ];
            if(!empty($_FILES['titlepic']['tmp_name'])) {
                if($f["size"] > 500000) {
                    header('Location: /admin.php?settings&error2=1#form2');
                    exit();
                }
                $mime = finfo_file($finfo, $_FILES['titlepic']['tmp_name']);
                if(isset($accepted_types[$mime])) {
                    $uploadfile = $upload_dir . $sep . basename('profile' . '.' . $accepted_types[$mime]);
                    move_uploaded_file($_FILES['titlepic']['tmp_name'], $uploadfile);
                }
            }
            $stmt = $dbh->prepare("UPDATE `settings` SET `title`=:title, `center_title`=:center_title, `subtitle`=:subtitle");
            try {
                $stmt->execute([
                    'title' => $title,
                    'center_title' => $center_title,
                    'subtitle' => $subtitle
                ]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error2#form2'); 
                break;
            }
            header('Location: /admin.php?settings&success2#form2'); 
            break;
        case 'contacts-update':
            $email = _post('email');
            $phone = _post('phone');
            $fax = _post('fax');
            $mobile = _post('mobile');
            $stmt = $dbh->prepare("UPDATE `settings` SET `email`=:email, `phone`=:phone, `fax`=:fax, `mobile`=:mobile");
            try {
                $stmt->execute([
                    'email' => $email,
                    'phone' => $phone,
                    'fax' => $fax,
                    'mobile' => $mobile
                ]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error3#form3'); 
                break;
            }
            header('Location: /admin.php?settings&success3#form3'); 
            break;
        case 'info-update':
            $address = _post('address');
            $descr = _post('descr');
            $stmt = $dbh->prepare("UPDATE `settings` SET `address`=:address, `short_descr`=:descr");
            try {
                $stmt->execute([
                    'address' => $address,
                    'descr' => $descr
                ]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error4#form4'); 
                break;
            }
            header('Location: /admin.php?settings&success4#form4'); 
            break;
        case 'social-update':
            $vk = _post('vk');
            $facebook = _post('facebook');
            $googleplus = _post('googleplus');
            $twitter = _post('twitter');
            $linkedin = _post('linkedin');

            $stmt = $dbh->prepare("UPDATE `settings` SET `vk`=:vk, `facebook`=:facebook, `googleplus`=:googleplus, `twitter`=:twitter, `linkedin`=:linkedin");
            try {
                $stmt->execute([
                    'vk' => $vk,
                    'facebook' => $facebook,
                    'googleplus' => $googleplus,
                    'twitter' => $twitter,
                    'linkedin' => $linkedin
                ]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error5#form5'); 
                break;
            }
            header('Location: /admin.php?settings&success5#form5'); 
            break;
        case 'descr-update':
            $left_descr = _post('left-descr');
            $right_descr = _post('right-descr');
            $stmt = $dbh->prepare("UPDATE `settings` SET `left_descr`=:left_descr, `right_descr`=:right_descr");
            try {
                $stmt->execute([
                    'left_descr' => $left_descr,
                    'right_descr' => $right_descr
                ]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error6#form6'); 
                break;
            }
            header('Location: /admin.php?settings&success6#form6'); 
            break;
        case 'activity-update':
            $counter = 0;
            $text1 = _post('desc1');
            $text2 = _post('desc2');
            $text3 = _post('desc3');
            $sep = DIRECTORY_SEPARATOR;
            $upload_dir = realpath(__DIR__ . $sep . '..' . $sep . 'img' . $sep . 'uploaded' . $sep);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $accepted_types = [
                'image/png' => 'png'
            ];
            $files = [
                $_FILES['img1'],
                $_FILES['img2'],
                $_FILES['img3'],
            ];
            foreach($files as $f) {
                $counter++;
                if(($counter > 3) || empty($f['tmp_name'])) continue;
                if($f["size"] > 500000) {
                    header('Location: /admin.php?settings&error7=1#form7');
                    exit();
                }
                $mime = finfo_file($finfo, $f['tmp_name']);
                if(isset($accepted_types[$mime])) {
                    $uploadfile = $upload_dir . $sep . basename('act' . $counter . '.' . $accepted_types[$mime]);
                    move_uploaded_file($f['tmp_name'], $uploadfile);
                }
            }
            $stmt = $dbh->prepare("UPDATE `settings` SET `act_text1`=:text1, `act_text2`=:text2, `act_text3`=:text3");
            try {
                $imgpath = '/img/uploaded/';
                $stmt->execute([
                    'text1' => $text1,
                    'text2' => $text2,
                    'text3' => $text3
                ]);
            }
            catch(PDOException $e) {
                header('Location: /admin.php?settings&error7#form7'); 
                break;
            }
            header('Location: /admin.php?settings&success7#form7'); 
            break;
        default:
            header('Location: /admin.php?settings');   
            break;
    }
}

$data = null;
$getData = $dbh->prepare("SELECT * FROM `settings` WHERE `id`='1';");
$getData->execute();
$datas = $getData->fetchAll();
foreach($datas as $v) {
  $data = $v;
}
?>
<div class="container flex" style="max-width:700px">
    <h3 id="form1">Основная конфигурация</h3>
    <form method="post" action="/admin/settings.php">
    <div class="form-group">
        <input type="hidden" name="actiontype" value="password-update">
        <label for="adminpw">Пароль админпанели</label>
        <input type="password" name="newpassword" class="form-control mb-2" id="adminpw" aria-describedby="pwhelp" placeholder="Введите новый пароль">
        <input type="password" name="newpassword2" class="form-control" id="adminpw2" aria-describedby="pwhelp" placeholder="Введите новый пароль ещё раз">
        <small id="pwhelp" class="form-text text-muted">Не ошибитесь при смене пароля</small>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error1')) he(' hidden'); ?>" role="alert">
            <? if(par('error1')) he('Ошибка смены пароля!'); ?>
            <? if(_get('error1') == '1') he('Пароли не совпадают!'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success1')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Сменить пароль</button>
    </form>
    <hr>
    <h3 id="form2">Заголовок сайта</h3>
    <form enctype="multipart/form-data" method="post" action="/admin/settings.php">
        <input type="hidden" name="actiontype" value="titles-update">
        <div class="form-group">
            <label for="title-text">Заголовок сайта</label>
            <input type="text" name="title" class="form-control" id="title-text" placeholder="Введите новый заголовок" value="<? he($data['title']); ?>">
        </div>
        <div class="form-group">
            <label for="center-title-text">Центральный заголовок сайта</label>
            <input type="text" name="center_title" class="form-control" id="center-title-text" placeholder="Введите новый заголовок" value="<? he($data['center_title']); ?>">
        </div>
        <div class="form-group">
            <label for="title-text">Подзаголовок сайта</label>
            <input type="text" name="subtitle" class="form-control" id="subtitle-text" placeholder="Введите новый подзаголовок" value="<? he($data['subtitle']); ?>">
        </div>
        <div class="form-group">
            <label>Картинка в заголовке</label>
            <div class="custom-file mb-2">
                <input type="file" name="titlepic" class="custom-file-input">
                <label class="custom-file-label" for="customFile">Выберите файл картинки...</label>
            </div>
        </div>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error2')) he(' hidden'); ?>" role="alert">
            <? 
                if(par('error2')) {
                    if(empty(_get('error2'))) he('Ошибка обновления данных!'); 
                    else he('Размер загружаемой картинки не должен быть больше 500 кБ.');
                }
            ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success2')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <hr>
    <h3 id="form7">Информация о деятельности</h3>
    <form enctype="multipart/form-data" method="post" action="/admin/settings.php">
        <input type="hidden" name="actiontype" value="activity-update">
        <div class="form-group">
            <label for="desc1">Текст к первой картинке</label>
            <textarea name="desc1" rows="3" class="form-control" placeholder="Введите первый текст"><? e($data['act_text1']) ?></textarea>
            <div class="custom-file my-2">
                <input type="file" name="img1" class="custom-file-input">
                <label class="custom-file-label" for="customFile">Выберите файл картинки...</label>
            </div>
        </div>
        <div class="form-group">
            <label for="desc1">Текст ко второй картинке</label>
            <textarea name="desc2" rows="3" class="form-control" placeholder="Введите первый текст"><? e($data['act_text2']) ?></textarea>
            <div class="custom-file my-2">
                <input type="file" name="img2" class="custom-file-input">
                <label class="custom-file-label" for="customFile">Выберите файл картинки...</label>
            </div>
        </div>
        <div class="form-group">
            <label for="desc1">Текст к третьей картинке</label>
            <textarea name="desc3" rows="3" class="form-control" placeholder="Введите первый текст"><? e($data['act_text3']) ?></textarea>
            <div class="custom-file my-2">
                <input type="file" name="img3" class="custom-file-input">
                <label class="custom-file-label" for="customFile">Выберите файл картинки...</label>
            </div>
        </div>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error7')) he(' hidden'); ?>" role="alert">
            <? 
                if(par('error7')) {
                    if(empty(_get('error7'))) he('Ошибка обновления данных!'); 
                    else he('Размер загружаемой картинки не должен быть больше 500 кБ.');
                }
            ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success7')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <hr>
    <h3 id="form6">Секция &laquo;О нас&raquo;</h3>
    <form method="post" action="/admin/settings.php">
        <input type="hidden" name="actiontype" value="descr-update">
        <div class="form-group">
            <label for="text1">Левый текст</label>
            <textarea class="form-control" name="left-descr" id="text1" rows="3"><? he($data['left_descr']); ?></textarea>
        </div>
        <div class="form-group">
            <label for="text2">Правый текст</label>
            <textarea class="form-control" name="right-descr" id="text2" rows="3"><? he($data['right_descr']); ?></textarea>
        </div>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error6')) he(' hidden'); ?>" role="alert">
            <? if(par('error6')) he('Ошибка обновления данных!'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success6')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <hr>
    <h3 id="form3">Контактные данные</h3>
    <form method="post" action="/admin/settings.php">
        <input type="hidden" name="actiontype" value="contacts-update">
        <div class="form-group">
            <label for="email-text">E-Mail</label>
            <input type="email" name="email" class="form-control" id="email-text" placeholder="Введите новый E-Mail" value="<? he($data['email']); ?>">
        </div>
        <div class="form-group">
            <label for="tel-text">Телефон</label>
            <input type="tel" name="phone" class="form-control" id="tel-text" placeholder="Введите новый телефон" value="<? he($data['phone']); ?>">
        </div>
        <div class="form-group">
            <label for="fax-text">Факс</label>
            <input type="tel" name="fax" class="form-control" id="fax-text" placeholder="Введите новый факс" value="<? he($data['fax']); ?>">
        </div>
        <div class="form-group">
            <label for="mob-text">Мобильный телефон</label>
            <input type="tel" name="mobile" class="form-control" id="mob-text" placeholder="Введите новый мобильный" value="<? he($data['mobile']); ?>">
        </div>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error3')) he(' hidden'); ?>" role="alert">
            <? if(par('error3')) he('Ошибка обновления данных!'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success3')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <hr>
    <h3 id="form4">Информация о компании</h3>
    <form method="post" action="/admin/settings.php">
        <input type="hidden" name="actiontype" value="info-update">
        <div class="form-group">
            <label for="address-text">Адрес компании</label>
            <textarea name="address" class="form-control" id="address-text" placeholder="Введите новый адрес" rows="3"><? he($data['address']); ?></textarea>
        </div>
        <div class="form-group">
            <label for="about-text">Краткое описание компании</label>
            <textarea name="descr" class="form-control" id="about-text" placeholder="Введите новое описание" rows="3"><? he($data['short_descr']); ?></textarea>
        </div>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error4')) he(' hidden'); ?>" role="alert">
            <? if(par('error4')) he('Ошибка обновления данных!'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success4')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <hr>
    <h3 id="form5">Ссылки на социальные сети</h3>
    <form method="post" action="/admin/settings.php">
        <input type="hidden" name="actiontype" value="social-update">
        <div class="form-group">
            <label for="title-text">ВКонтакте</label>
            <input type="text" name="vk" class="form-control" id="vk-text" placeholder="Введите новую ссылку" value="<? he($data['vk']); ?>">
        </div>
        <div class="form-group">
            <label for="title-text">Facebook</label>
            <input type="text" name="facebook" class="form-control" id="fb-text" placeholder="Введите новую ссылку" value="<? he($data['facebook']); ?>">
        </div>
        <div class="form-group">
            <label for="title-text">Google+</label>
            <input type="text" name="googleplus" class="form-control" id="gplus-text" placeholder="Введите новую ссылку" value="<? he($data['googleplus']); ?>">
        </div>
        <div class="form-group">
            <label for="title-text">Twitter</label>
            <input type="text" name="twitter" class="form-control" id="twitter-text" placeholder="Введите новую ссылку" value="<? he($data['twitter']); ?>">
        </div>
        <div class="form-group">
            <label for="title-text">LinkedIn</label>
            <input type="text" name="linkedin" class="form-control" id="linkedin-text" placeholder="Введите новую ссылку" value="<? he($data['linkedin']); ?>">
            <small id="socialhelp" class="form-text text-muted">Если поле не заполнено - иконка пустого поля не появится на сайте!</small>
        </div>
        <div class="alert alert-danger alert-dismissible fade show<? if(!par('error5')) he(' hidden'); ?>" role="alert">
            <? if(par('error5')) he('Ошибка обновления данных!'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show<? if(!par('success5')) he(' hidden'); ?>">
            Операция успешно выполнена
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <hr class="mb-3">
</div>