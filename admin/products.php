<?
session_start();
include_once __DIR__ . '/checkauth.php';
include_once __DIR__ . '/../config/db.php';
include_once __DIR__ . '/../functions.php';

if(!is_authorized($dbh)) {
    header('Location: /auth.php');
    exit();
}
?>

<div style="width:100%; height:100%" id="product-app">
    <div class="loader-bg" v-if="submitting == true">
        <div class="loader"></div>
    </div>
    <div class="form-group admin-cat-switcher" v-if="editing == false">
        <label for="category-select">Выбор категории</label>
        <div class="d-flex flex-row align-center justify-center mb-1">
            <select class="form-control" id="category-select" @change="refreshProducts" v-model:value="category">
                <option value="0">Любая</option>
                <option v-for="cat in categories" :key="cat.id" v-bind:value="cat.id">{{cat.name}}</option>
            </select>
        </div>
        <div class="admin-card p-2 d-flex flex-row align-items-center justify-content-center my-1">
            <button class="btn btn-info mx-1" @click="showEditingOrUpdateForm">Добавить новый продукт</button>
            <button class="btn btn-success mx-1" @click="showEditingOrUpdateCategory(false)">Добавить категорию</button>
            <button class="btn btn-primary mx-1" @click="showEditingOrUpdateCategory(true)" v-if="category != 0">Изменить категорию</button>
            <button class="btn btn-danger mx-1" @click="deleteCategory()" v-if="category != 0">Удалить категорию</button>
        </div>
    </div>
    <div class="admin-card product" v-for="product in products" :key="product.id" v-if="editing == false">
        <table class="table">
            <tbody>
                <tr>
                    <th scope="row">Название</th>
                    <td>{{product.name}}</td>
                </tr>
                <tr>
                    <th scope="row">Категория</th>
                    <td>{{product.category.name}}</td>
                </tr>
                <tr>
                    <th scope="row">Цена за шт.</th>
                    <td>{{product.cost}}</td>
                </tr>
                <tr>
                    <th scope="row">Количество</th>
                    <td>{{product.count}}</td>
                </tr>
            </tbody>
        </table>
        <div class="w-100 d-flex flex-row align-center justify-center">
            <button class="mx-1 btn btn-success" @click="showEditingOrUpdateForm(product.id)">Редактировать продукт</button>
            <button class="mx-1 btn btn-danger" @click="deleteProduct(product.id)">Удалить продукт</button>
        </div>
    </div>

    <div class="d-flex flex-row align-items-center justify-content-center" v-if="editing == false">
        <nav aria-label="Навигация">
            <ul class="pagination">
                <li v-bind:class="[page == 1 ? 'page-item disabled' : 'page-item']"><a class="page-link" @click="previousPage">&lt;&lt; Назад</a></li>
                <li v-bind:class="[page == totalPages ? 'page-item disabled' : 'page-item']"><a class="page-link" @click="nextPage">Вперёд &gt;&gt;</a></li>
            </ul>
        </nav>
    </div>

    <div class="container" v-if="editingProduct == true">
        <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12">
                <div class="form-group">
                    <label for="product-name">Название продукта</label>
                    <input type="text" class="form-control" id="product-name" v-model="editors.product.name" placeholder="Введите название продукта">
                </div>
                <div class="form-group">
                    <label for="category-select">Категория продукта</label>
                    <select class="form-control" v-model="editors.product.category" @change="productEditorCategoryChange">
                        <option disabled value="">Выберите категорию</option>
                        <option v-for="cat in categories" :key="cat.id" v-bind:value="cat.id">{{cat.name}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Картинка</label>
                    <div class="custom-file mb-2">
                        <input type="file" name="imgfile" class="custom-file-input" @change="catchImgChange">
                        <label class="custom-file-label" for="customFile">Выберите файл картинки...</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="product-cost">Цена продукта</label>
                    <input type="number" class="form-control" id="product-cost" v-model="editors.product.cost">
                </div>
                <div class="form-group">
                    <label for="product-count">Количество продукта</label>
                    <input type="number" class="form-control" id="product-coount" v-model="editors.product.count">
                </div>
                <button class="btn btn-success" @click="editProduct">Сохранить</button>
                <button class="btn" @click="cancelEditingOrUpdate">Отменить</button>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12">
                <div class="product-card disable-flip m-2">
                    <div class="product-card__face product-card-front" v-bind:style="{'background-image': 'url(' + editors.product.img_data + ')'}"></div>
                    <div class="product-card__face product-card-back">
                    <h2 class="product-card-text product-card-name">{{editors.product.name}}</h2>
                    <p class="product-card-text product-card-category">{{editors.product.categoryName}}</p>
                    <p class="product-card-text product-card-cost"><span>Цена:</span> {{editors.product.cost}}</p>
                    <button class="btn btn-sm mt-3 btn-outline-success">В корзину</button>
                    </div>
                </div>
                <div class="product-card card-flipped m-2">
                    <div class="product-card__face product-card-front"></div>
                    <div class="product-card__face product-card-back">
                    <h2 class="product-card-text product-card-name">{{editors.product.name}}</h2>
                    <p class="product-card-text product-card-category">{{editors.product.categoryName}}</p>
                    <p class="product-card-text product-card-cost"><span>Цена:</span> {{editors.product.cost}}</p>
                    <button class="btn btn-sm mt-3 btn-outline-success">В корзину</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="admin-cat-switcher" v-if="editingCategory == true">
        <div class="form-group">
            <label for="cat-name">Название категории</label>
            <input type="text" class="form-control" id="cat-name" v-model="editors.category.name">
        </div>
        <button class="btn btn-success" @click="editCategory">Сохранить</button>
        <button class="btn" @click="cancelEditingOrUpdate">Отменить</button>
    </div>
</div>