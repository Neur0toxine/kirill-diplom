<?
session_start();
include_once __DIR__ . '/checkauth.php';
include_once __DIR__ . '/../config/db.php';
include_once __DIR__ . '/../functions.php';

if (!is_authorized($dbh)) {
    header('Location: /auth.php');
    exit();
}
?>

<div style="width:100%; height:100%" id="order-app">
    <div class="loader-bg" v-if="submitting == true">
        <div class="loader"></div>
    </div>
    <div class="form-group admin-cat-switcher" v-if="editing == false">
        <label for="status-select">Выбор категории</label>
        <div class="d-flex flex-row align-center justify-center mb-1">
            <select class="form-control" id="status-select" @change="refreshOrders" v-model:value="filter">
                <option value="-1">Любая</option>
                <option value="0">Открытые заказы</option>
                <option value="1">Завершённые заказы</option>
            </select>
        </div>
    </div>
    <div class="admin-card product" v-for="order in orders" :key="order.id">
        <table class="table">
            <tbody>
                <tr>
                    <th scope="row">Дата регистрации</th>
                    <td>{{order.created}}</td>
                </tr>
                <tr>
                    <th scope="row">Имя заказчика</th>
                    <td>{{order.customer_name}}</td>
                </tr>
                <tr>
                    <th scope="row">E-Mail заказчика</th>
                    <td>{{order.customer_email}}</td>
                </tr>
                <tr>
                    <th scope="row">Заказанные продукты</th>
                    <td>
                        <div class="ordered-item" v-for="product in order.products" :key="product.name">
                            {{product.name}}, категория "{{product.category}}", {{product.cost}}/шт., {{product.count}} шт. всего, {{product.price}} руб.
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Статус заказа</th>
                    <td>Заказ {{order.status == 0 ? 'открыт' : 'закрыт'}}.</td>
                </tr>
                <tr>
                    <th scope="row">Итого</th>
                    <td>{{order.total}} руб.</td>
                </tr>
            </tbody>
        </table>
        <div class="w-100 d-flex flex-row align-center justify-center">
            <button class="mx-1 btn btn-success" v-if="order.status == 0" @click="closeOrder(order.id)">Закрыть заказ</button>
            <button class="mx-1 btn btn-danger" @click="removeOrder(order.id)">Удалить заказ</button>
        </div>
    </div>

    <div class="d-flex flex-row align-items-center justify-content-center">
        <nav aria-label="Навигация">
            <ul class="pagination">
                <li v-bind:class="[page == 1 ? 'page-item disabled' : 'page-item']"><a class="page-link" @click="previousPage">&lt;&lt; Назад</a></li>
                <li v-bind:class="[page == totalPages ? 'page-item disabled' : 'page-item']"><a class="page-link" @click="nextPage">Вперёд &gt;&gt;</a></li>
            </ul>
        </nav>
    </div>
</div>
