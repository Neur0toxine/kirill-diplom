new Vue({
  el: '#main-cat-container',
  data: function() {
    return {
      checkout: 0,
      checkout_success: true,
      cName: '',
      cEmail: '',
      cPhone: '',
      catClass: 'list-group-item cat-list-item',
      category: 0,
      categories: [],
      products: [],
      cart: [],
      submitting: false,
      page: 1,
      limit: 20,
      totalPages: 1,
      totalProducts: 1
    }
  },
  mounted: function() {
    console.log('Catalog page mounted');
    $(document).ajaxStart((function() {
        this.$data.submitting = true;
    }).bind(this));
    $(document).ajaxComplete((function() {
        this.$data.submitting = false;
    }).bind(this));
    this.$data.cart = JSON.parse(localStorage.getItem('cart') || "[]");
    this.refreshCategories();
    this.refreshProducts();
  },
  computed: {
      total: function() {
          var result = 0;
          this.$data.cart.forEach((function(x){
              result += (x.cost * x.count);
          }).bind(this));
          return result;
      }
  },
  methods: {
    addToCart: function(id) {
        var cart_product = this.$data.cart.filter(function(x) {
            return x.id == id;
        });
        if(cart_product.length > 0) {
            this.$data.cart = this.$data.cart.map(function(x) {
                return x.id == id ? {...x, count: x.count + 1} : x;
            });
        } else {
            var product = this.$data.products.map(function(x) {
                return x.id == id ? {...x, count: 1} : null;
            }).filter(function(x){ return x != null; });

            if(product.length > 0) {
                this.$data.cart.push(product[0]);
            }
        }
        this.saveCart();
    },
    removeFromCart: function(id, all) {
        if(all) {
            this.$data.cart = this.$data.cart.filter(function(x) {
                return x.id != id;
            });
        } else {
            this.$data.cart = this.$data.cart.map(function(x) {
                return x.id == id ? {...x, count: x.count - 1} : x;
            });
        }
        console.log(this.$data.cart.length);
        if(this.$data.cart.length == 0) this.showCheckout(0);
        this.saveCart();
    },
    saveCart: function() {
        localStorage.setItem('cart', JSON.stringify(this.$data.cart));
    },
    showCheckout: function(flag) {
        if(this.$data.cart.length == 0 && flag != 0) {
            alert('Добавьте хотя бы один продукт в заказ!');
            return;
        }
        this.$data.checkout = typeof flag === 'undefined' ? 0 : flag;
    },
    sendOrder: function() {
        $.ajax('/api/orders.php', {
            data: JSON.stringify({
                action: 'create',
                data: {
                    name: this.$data.cName,
                    email: this.$data.cEmail,
                    phone: this.$data.cPhone,
                    products: this.$data.cart
                }
            }),
            contentType: 'application/json',
            type: 'POST',
            async: true
        })
        .then((function(ret) {
            this.$data.checkout_success = ret.success || false;
            this.showCheckout(3);
            this.$data.cart = [];
            this.$data.cName = '';
            this.$data.cEmail = '';
            this.$data.cPhone = '';
            localStorage.removeItem('cart');
            setTimeout((function(){
                this.showCheckout(0);
            }).bind(this), 3000);
        }).bind(this));
    },
    switchCat: function(cat) {
      if(this.$data.category == cat) return false;
      this.$data.category = cat;
      this.refreshProducts();
    },
    refreshProducts: function() {
      var for_send = {
          page: this.$data.page,
          limit: this.$data.limit
      };
      if(this.$data.category != 0) {
          for_send.category = this.$data.category;
      }
      $.ajax('/api/products.php', {
          data: JSON.stringify({
              action: 'read',
              data: for_send
          }),
          contentType: 'application/json',
          type: 'POST',
          async: true
      }).then((function(data) {
          if(data.error) {
              console.log('error found');
          } else {
              this.$data.page = data.page;
              this.$data.limit = data.limit;
              this.$data.totalPages = data.totalPages;
              this.$data.totalProducts = data.totalItems;
              this.$data.products = data.items;
          }
      }).bind(this)).catch((function(err) {
          console.log(err);
      }).bind(this));
    },
    refreshCategories: function() {
        $.ajax('/api/products.php', {
            data: JSON.stringify({
                action: 'cat_read',
                data: {}
            }),
            contentType: 'application/json',
            type: 'POST',
            async: true
        }).then((function(data){
            this.$data.categories = data.items;
        }).bind(this));
    },
    nextPage: function() {
        if(this.$data.page != this.$data.totalPages) {
            this.$data.page++;
            this.refreshProducts();
        }
    },
    previousPage: function() {
        if(this.$data.page != 1) {
            this.$data.page--;
            this.refreshProducts();
        }
    }
  }
});