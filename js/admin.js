'use strict';
(function () {
    $(document).ready(function () {
        $('.admin-card.feedback .closebtn').on('click', function () {
            var $id = $(this).parent('div').data('id');
            if (confirm('Вы действительно хотите удалить эту запись?')) {
                $.ajax({
                    url: '/admin/feedback.php',
                    type: 'DELETE',
                    data: {
                        id: $id
                    },
                    success: function (result) {
                        $('div[data-id="' + $id + '"]').fadeOut(300, function () {
                            $(this).remove();
                        });
                        console.log(result);
                    },
                    error: function (result) {
                        alert('Не удалось удалить запись');
                    }
                });
            }
        });
        $('.admin-card.contact .closebtn').on('click', function () {
            var $id = $(this).parent('div').data('id');
            if (confirm('Вы действительно хотите удалить эту запись?')) {
                $.ajax({
                    url: '/admin/contact.php',
                    type: 'DELETE',
                    data: {
                        id: $id
                    },
                    success: function (result) {
                        $('div[data-id="' + $id + '"]').fadeOut(300, function () {
                            $(this).remove();
                        });
                        console.log(result);
                    },
                    error: function (result) {
                        alert('Не удалось удалить запись');
                    }
                });
            }
        });
    });

    let loc = window.location.search.match(/(?:\w+)/);
    switch (loc !== null ? loc[0] : '') {
        case 'products':
            new Vue({
                data: function () {
                    return {
                        editing: false,
                        editingProduct: false,
                        editingCategory: false,
                        submitting: false,
                        editors: {
                            product: {
                                id: 0,
                                name: '',
                                category: 1,
                                categoryName: '',
                                cost: 1000,
                                count: 10,
                                imgsrc: '',
                                img_data: ''
                            },
                            category: {
                                id: 0,
                                name: ''
                            }
                        },
                        products: [],
                        categories: [],
                        category: 0,
                        page: 1,
                        totalPages: 1,
                        totalItems: 1,
                        limit: 15
                    }
                },
                mounted: function () {
                    console.log('Products app mounted.');
                    $(document).ajaxStart((function () {
                        this.$data.submitting = true;
                    }).bind(this));
                    $(document).ajaxComplete((function () {
                        this.$data.submitting = false;
                    }).bind(this));
                    this.refreshCategories();
                    this.refreshProducts();
                },
                methods: {
                    editProduct: function (data) {
                        var formURL = '/api/products.php';
                        var imgfile = $('input[name="imgfile"]')[0];
                        var sendProduct = (function () {
                            var send_action = this.$data.editors.product.id == 0 ? 'create' : 'update';
                            var proddata = {
                                id: this.$data.editors.product.id,
                                name: this.$data.editors.product.name,
                                category: this.$data.editors.product.category,
                                cost: this.$data.editors.product.cost,
                                count: this.$data.editors.product.count
                            };
                            if (this.$data.editors.product.imgsrc != '') {
                                proddata.imgsrc = this.$data.editors.product.imgsrc;
                            }
                            $.ajax(formURL, {
                                data: JSON.stringify({
                                    action: send_action,
                                    data: proddata
                                }),
                                contentType: 'application/json',
                                type: 'POST',
                                async: true
                            }).then((function (res) {
                                this.cancelEditingOrUpdate();
                                this.refreshProducts();
                            }).bind(this));
                        }).bind(this);
                        if (imgfile.files.length == 0 && this.$data.editors.product.imgsrc == '') {
                            return false;
                        }
                        if (imgfile.files.length != 0) {
                            var img = new FormData();
                            img.append('imgfile', imgfile.files[0]);
                            if (this.$data.editors.product.imgsrc != '') {
                                img.append('img_name', this.$data.editors.product.imgsrc.replace(/^([\/\w]+\/)|(\.png)/g, ''));
                            }
                            $.ajax({
                                    url: formURL,
                                    type: "POST",
                                    data: img,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    async: true
                                })
                                .then((function (res) {
                                    if (res.toString().startsWith('ok')) {
                                        this.$data.editors.product.imgsrc = res.toString().replace('ok:', '');
                                        sendProduct();
                                    }
                                }).bind(this));
                        } else sendProduct();
                    },
                    deleteProduct: function (id) {
                        if (confirm('Удалить данные этого продукта?')) {
                            $.ajax('/api/products.php', {
                                    data: JSON.stringify({
                                        action: 'delete',
                                        data: {
                                            id: id
                                        }
                                    }),
                                    contentType: 'application/json',
                                    type: 'POST',
                                    async: true
                                })
                                .then((function (ret) {
                                    if (ret.success == false) {
                                        alert('Не удалось удалить продукт!');
                                    } else {
                                        alert('Продукт успешно удалён.');
                                        this.refreshProducts();
                                    }
                                }).bind(this));
                        }
                    },
                    editCategory: function () {
                        var send_action = this.$data.editors.category.id == 0 ? 'cat_create' : 'cat_update';
                        $.ajax('/api/products.php', {
                            data: JSON.stringify({
                                action: send_action,
                                data: {
                                    id: this.$data.editors.category.id,
                                    name: this.$data.editors.category.name,
                                }
                            }),
                            contentType: 'application/json',
                            type: 'POST',
                            async: true
                        }).then((function (res) {
                            this.cancelEditingOrUpdate();
                            this.refreshCategories();
                            this.refreshProducts();
                        }).bind(this));
                    },
                    deleteCategory: function () {
                        if (confirm('Удалить эту категорию со всеми продуктами?')) {
                            $.ajax('/api/products.php', {
                                    data: JSON.stringify({
                                        action: 'cat_delete',
                                        data: {
                                            id: this.$data.category
                                        }
                                    }),
                                    contentType: 'application/json',
                                    type: 'POST',
                                    async: true
                                })
                                .then((function (ret) {
                                    if (ret.success == false) {
                                        alert('Не удалось удалить категорию!');
                                    } else {
                                        alert('Категория успешно удалена.');
                                        this.$data.category = 0;
                                        this.refreshCategories();
                                        this.refreshProducts();
                                    }
                                }).bind(this));
                        }
                    },
                    cancelEditingOrUpdate: function () {
                        this.$data.editors.product = {
                            id: 0,
                            name: '',
                            category: 1,
                            categoryName: '',
                            cost: 1000,
                            count: 10,
                            imgsrc: '',
                            img_data: ''
                        };
                        this.$data.editors.category = {
                            id: 0,
                            name: ''
                        };
                        this.$data.editingProduct = false;
                        this.$data.editingCategory = false;
                        this.$data.editing = false;
                    },
                    showEditingOrUpdateCategory: function (isEditing) {
                        if (isEditing) {
                            var cat = this.$data.categories.filter((function (category) {
                                return category.id == this.$data.category;
                            }).bind(this))[0];
                            this.$data.editors.category = {
                                id: cat.id,
                                name: cat.name
                            };
                        }
                        this.$data.editing = true;
                        this.$data.editingCategory = true;
                    },
                    showEditingOrUpdateForm: function (id) {
                        var prod_el = this.$data.products.filter(function (item) {
                            return item.id == id
                        })[0];
                        if (typeof prod_el !== 'undefined') {
                            this.$data.editors.product = {
                                id: prod_el.id,
                                name: prod_el.name,
                                category: prod_el.category.id,
                                cost: prod_el.cost,
                                count: prod_el.count,
                                imgsrc: prod_el.imgsrc,
                                img_data: prod_el.imgsrc
                            };
                        }
                        this.productEditorCategoryChange();
                        this.$data.editing = true;
                        this.$data.editingProduct = true;
                    },
                    refreshProducts: function () {
                        var for_send = {
                            page: this.$data.page,
                            limit: this.$data.limit
                        };
                        if (this.$data.category != 0) {
                            for_send.category = this.$data.category;
                        }
                        $.ajax('/api/products.php', {
                            data: JSON.stringify({
                                action: 'read',
                                data: for_send
                            }),
                            contentType: 'application/json',
                            type: 'POST',
                            async: true
                        }).then((function (data) {
                            if (data.error) {
                                console.log('error found');
                            } else {
                                this.$data.page = data.page;
                                this.$data.limit = data.limit;
                                this.$data.totalPages = data.totalPages;
                                this.$data.totalItems = data.totalItems;
                                this.$data.products = data.items;
                            }
                        }).bind(this)).catch((function (err) {
                            console.log(err);
                        }).bind(this));
                    },
                    refreshCategories: function () {
                        $.ajax('/api/products.php', {
                            data: JSON.stringify({
                                action: 'cat_read',
                                data: {}
                            }),
                            contentType: 'application/json',
                            type: 'POST',
                            async: true
                        }).then((function (data) {
                            this.$data.categories = data.items;
                        }).bind(this));
                    },
                    catchImgChange: function () {
                        var img_input = $('input[name="imgfile"]')[0];
                        if (img_input.files && img_input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = (function (e) {
                                this.$data.editors.product.img_data = e.target.result;
                            }).bind(this);
                            reader.readAsDataURL(img_input.files[0]);
                        }
                    },
                    nextPage: function () {
                        if (this.$data.page != this.$data.totalPages) {
                            this.$data.page++;
                            this.refreshProducts();
                        }
                    },
                    previousPage: function () {
                        if (this.$data.page != 1) {
                            this.$data.page--;
                            this.refreshProducts();
                        }
                    },
                    productEditorCategoryChange: function () {
                        this.$data.editors.product.categoryName = this.$data.categories.filter((function (x) {
                            return x.id == this.$data.editors.product.category
                        }).bind(this))[0].name;
                    }
                }
            }).$mount('#product-app');
            break;
        case 'orders':
            new Vue({
                data: function () {
                    return {
                        editing: false,
                        submitting: false,
                        orders: [],
                        filter: -1,
                        page: 1,
                        totalPages: 1,
                        totalItems: 1,
                        limit: 15
                    }
                },
                mounted: function () {
                    console.log('Orders app mounted.');
                    $(document).ajaxStart((function () {
                        this.$data.submitting = true;
                    }).bind(this));
                    $(document).ajaxComplete((function () {
                        this.$data.submitting = false;
                    }).bind(this));
                    this.refreshOrders();
                },
                methods: {
                    closeOrder: function (id) {
                        if (confirm('Завершить этот заказ?')) {
                            $.ajax('/api/orders.php', {
                                    data: JSON.stringify({
                                        action: 'update',
                                        data: {
                                            id: id,
                                            status: 1
                                        }
                                    }),
                                    contentType: 'application/json',
                                    type: 'POST',
                                    async: true
                                })
                                .then((function (ret) {
                                    if (ret.success == false) {
                                        alert('Не удалось изменить статус заказа!');
                                    } else {
                                        alert('Заказ закрыт.');
                                        this.refreshOrders();
                                    }
                                }).bind(this));
                        }
                    },
                    removeOrder: function (id) {
                        if (confirm('Удалить данные этого заказа?')) {
                            $.ajax('/api/orders.php', {
                                    data: JSON.stringify({
                                        action: 'delete',
                                        data: {
                                            id: id
                                        }
                                    }),
                                    contentType: 'application/json',
                                    type: 'POST',
                                    async: true
                                })
                                .then((function (ret) {
                                    if (ret.success == false) {
                                        alert('Не удалось удалить заказ!');
                                    } else {
                                        alert('Заказ успешно удалён.');
                                        this.refreshOrders();
                                    }
                                }).bind(this));
                        }
                    },
                    refreshOrders: function () {
                        var for_send = {
                            page: this.$data.page,
                            limit: this.$data.limit
                        };
                        if (this.filter != -1) {
                            for_send.status = +this.filter;
                        }
                        $.ajax('/api/orders.php', {
                            data: JSON.stringify({
                                action: 'read',
                                data: for_send
                            }),
                            contentType: 'application/json',
                            type: 'POST',
                            async: true
                        }).then((function (data) {
                            if (data.error) {
                                console.log('error found');
                            } else {
                                this.$data.page = data.page;
                                this.$data.limit = data.limit;
                                this.$data.totalPages = data.totalPages;
                                this.$data.totalItems = data.totalItems;
                                this.$data.orders = data.items;
                            }
                        }).bind(this)).catch((function (err) {
                            console.log(err);
                        }).bind(this));
                    },
                    nextPage: function () {
                        if (this.$data.page != this.$data.totalPages) {
                            this.$data.page++;
                            this.refreshOrders();
                        }
                    },
                    previousPage: function () {
                        if (this.$data.page != 1) {
                            this.$data.page--;
                            this.refreshOrders();
                        }
                    }
                }
            }).$mount('#order-app');
            break;
    }
})();