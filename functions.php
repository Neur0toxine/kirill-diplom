<?
function par($var) {
    return (isset($_GET[$var]) || isset($_POST[$var]));
}

function _get($var) {
    if(isset($_GET[$var])) return $_GET[$var];
    else return '';
}

function _post($var) {
    if(isset($_POST[$var])) return $_POST[$var];
    else return '';
}

function e($var) {
    echo $var;
}

function h($var) {
    return htmlspecialchars($var);
}

function he($var) {
    e(h($var));
}

function is_session_started()
{
    if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
            return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
        } else {
            return session_id() === '' ? FALSE : TRUE;
        }
    }
    return FALSE;
}

function format_phone($var) {
    return preg_replace('~.*(\+?\d{1,4})(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '$1 ($2) $3-$4', $var);
}
?>